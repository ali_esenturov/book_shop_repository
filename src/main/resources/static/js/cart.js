async function changeAmount(id){
    const action = id.split("_")[0];
    const book_id = id.replace(action + '_', '');
    const amount = parseInt(document.getElementById("amount_" + book_id).innerHTML.trim());
    const cart_id = document.getElementById('cart_id').value;
    let data = new FormData(document.getElementById("csrf_form"));

    let newAmount = 0;
    if(action === "plus"){
        newAmount = amount + 1;
        document.getElementById("amount_" + book_id).innerHTML = newAmount.toString();
        await fetch('cart/change/' + cart_id + '/' + book_id.replace('_','') + '/' + action,
            {method: 'POST', body: data});
    }
    else{
        if(amount === 1){
            newAmount = 1;
        }
        else{
            newAmount = amount - 1;
            await fetch('cart/change/' + cart_id + '/' + book_id.replace('_','') + '/' + action,
                {method: 'POST', body: data});
        }
        document.getElementById("amount_" + book_id).innerHTML = newAmount.toString();
    }
}

async function redirectToMainPage(){
    window.location.href = "/";
}

async function makeOrder(){
    let cart_id = document.getElementById('cart_id').value;
    let data = new FormData(document.getElementById("csrf_form"));

    await fetch('cart/make/order/' + cart_id,
        {method: 'POST', body: data});
    window.location.href = "/profile";
}

function countTotal(){
    let total_sum = 0
    let prices = document.getElementsByClassName("price");
    for(let i = 0; i < prices.length; i++){
        let book_id = prices[i].id.replace('price_', '');
        let amount = parseInt(document.getElementById('amount_' + book_id).innerHTML.trim());
        let priceDigit = prices[i].innerHTML.replace('Price:',"").replace('som',"");
        let sub_total = amount * parseInt(priceDigit);
        total_sum += sub_total;
    }
    document.getElementById("total_sum").innerHTML = total_sum.toString();
}

async function removeItem(id){
    let book_id = parseInt(id.replace('remove_', '').trim());
    let cart_id = document.getElementById('cart_id').value;
    let data = new FormData(document.getElementById("csrf_form"));

    await fetch('cart/delete/' + cart_id + '/' + book_id,
        {method: 'POST', body: data});

    let element = document.getElementById(book_id.toString());
    element.parentNode.removeChild(element);

    alert("Item was deleted from the cart");
}