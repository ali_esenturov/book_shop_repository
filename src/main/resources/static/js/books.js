'use strict';

async function searchBook(form){
    let data = new FormData(form);
    let book_name = data.get("name");
    let category_id = data.get("category_id");

    let url = 'http://localhost:9889/api/categories/search_book/' + category_id + '/' + book_name;
    let booksData = await fetch(url).then(b => b.json());

    let bookList = document.getElementById("itemList");
    bookList.innerHTML = createBookElements(booksData).innerHTML;
}

function createBookElements(books) {
    let elements = document.createElement('div');
    for(let i=0; i<books.length; i++){
        console.log(i);
        let book = new Book(books[i].id, books[i].name);
        let elem = createBookElement(book);
        elements.appendChild(elem);
    }
    return elements;
}

function createBookElement(book){
    let book_content = `<a href="/books/${book.id}"> ${book.name} </a>`;
    let book_el = document.createElement(`div`);
    book_el.innerHTML += book_content;
    book_el.classList.add("flex", "flex-column", "box", "flex-v-center");
    return book_el;
}

class Book {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}

async function redirectToMainPage(){
    window.location.href = "/";
}

async function addToCart(form) {
    const user_status = document.getElementById("user_status");
    if(user_status.value === "authorizedUser"){
        const options = {
            method : 'post',
            body: new FormData(form)
        };
        await fetch("/cart", options);
        alert("added into cart successfully");
    }
    else{
        document.location.href = "/login";
    }
}