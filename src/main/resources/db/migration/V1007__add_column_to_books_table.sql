use bookshop;

ALTER TABLE books
    ADD description varchar(250)
        AFTER name;