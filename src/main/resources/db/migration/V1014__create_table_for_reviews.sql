use bookshop;

create table `reviews` (
                          `id` INT auto_increment NOT NULL,
                          PRIMARY KEY (`id`),
                          `user_id` int not null,
                          CONSTRAINT `fk_review_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
                          `book_id` int not null,
                          CONSTRAINT `fk_review_book` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
                          `text` varchar(250) not null
);