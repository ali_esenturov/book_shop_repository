use bookshop;

ALTER TABLE users
    MODIFY email varchar(128) UNIQUE;