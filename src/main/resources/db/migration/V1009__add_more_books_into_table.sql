use bookshop;

insert into `books`(`name`,`image`,`price`,`book_category_id`,`author_id`)
values ('If It Bleeds', 'ifItBleeds.png', 300, 8, 4),
       ('Where the Crawdads Sing', 'whereTheCrawdadsSing.png', 350, 8, 2),
       ('Masked Prey', 'maskedPrey.png', 250, 8, 1),
       ('American Dirt', 'americanDirt.png', 280, 8, 3),
       ('The Book of Longings', 'theBookOfLongings.png', 340, 8, 3);