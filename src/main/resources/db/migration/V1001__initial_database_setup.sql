use `bookshop`;

CREATE TABLE `book_categories` (
                          `id` INT auto_increment NOT NULL,
                          `name` varchar(128) NOT NULL,
                          `description` varchar(128) NOT NULL,
                          PRIMARY KEY (`id`)
);

CREATE TABLE `authors` (
                           `id` INT auto_increment NOT NULL,
                           `full_name` varchar(128) NOT NULL,
                           `image` varchar(128) NOT NULL,
                           `age` int NOT NULL,
                           `short_biography` varchar(128) NOT NULL,
                           PRIMARY KEY (`id`)
);

create table `books` (
                         `id` INT auto_increment NOT NULL,
                         `name` varchar(128) NOT NULL,
                         `image` varchar(128) NOT NULL,
                         `price` float not null,
                         PRIMARY KEY (`id`),
                         `author_id` int not null,
                         CONSTRAINT `fk_book_author` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`),
                         `book_category_id` int not null,
                         CONSTRAINT `fk_book_category` FOREIGN KEY (`book_category_id`) REFERENCES `book_categories` (`id`)
);