use bookshop;

create table `orders` (
                         `id` INT auto_increment NOT NULL,
                         PRIMARY KEY (`id`),
                         `user_id` int not null,
                         CONSTRAINT `fk_order_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

create table `orders_books` (
                               `id` INT auto_increment NOT NULL,
                               `amount` INT not null,
                               `order_time` DATETIME not null,
                               PRIMARY KEY (`id`),
                               `order_id` int not null,
                               CONSTRAINT `fk_ob_order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
                               `book_id` int not null,
                               CONSTRAINT `fk_ob_book` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`)
);