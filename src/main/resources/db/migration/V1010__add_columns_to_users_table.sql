use bookshop;

ALTER TABLE users
    ADD enabled tinyint
        AFTER birth_date;

ALTER TABLE users
    ADD role varchar(128)
        AFTER enabled;