use `bookshop`;

insert into `book_categories` (`name`, `description`) values ('Action and Adventure', 'usually show an event or a series of events that happen.'),
                                                             ('Anthology', 'collection of series of works such as short stories, poems etc.'),
                                                             ('Classic', 'refers to the fictions that are accepted as the most important'),
                                                             ('Comic and Graphic Novel', 'These books are based on a sequence of hand-drawn pictures.'),
                                                             ('Crime and Detective', 'deals with crime, criminal motives and the investigation'),
                                                             ('Drama', 'Dramas are stories composed in verse or prose, usually for theatrical performance.'),
                                                             ('Fable', 'fictional narratives in prose or verse that typically features personified animals.'),
                                                             ('Fairy Tale', 'usually a story for children that involves imaginary creatures and magic.');