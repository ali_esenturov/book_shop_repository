use bookshop;

insert into `books`(`name`,`image`,`price`,`book_category_id`,`author_id`,`shop_id`)
values ('Harry Potter', 'harryPotter.png', 300, 8, 4, 1),
       ('Alma mater', 'almaMatter.png', 250, 3, 2, 1),
       ('The Hobbit The Lord of the Rings', 'theHobbit.png', 350, 8, 2, 1),
       ('A Tale of Two Cities', 'taleOfTwoCities.png', 155, 4, 3, 2),
       ('Bleak House', 'bleakHouse.png', 275, 5, 3, 3),
       ('The Dark Tower', 'theDarkTower.png', 180, 7, 1, 2),
       ('The Dead Zone', 'theDeadZone.png', 280, 7, 1, 3);