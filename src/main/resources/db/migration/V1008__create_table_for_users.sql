use bookshop;

CREATE TABLE `users` (
                         `id` INT auto_increment NOT NULL,
                         `name` varchar(128) NOT NULL,
                         `email` varchar(128) NOT NULL,
                         `password` varchar(128) NOT NULL,
                         `birth_date` date NOT NULL,
                         PRIMARY KEY (`id`)
);