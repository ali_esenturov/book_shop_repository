use bookshop;

create table `carts` (
                 `id` INT auto_increment NOT NULL,
                 PRIMARY KEY (`id`),
                 `user_id` int not null,
                 CONSTRAINT `fk_cart_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

create table `carts_books` (
                         `id` INT auto_increment NOT NULL,
                         `amount` INT not null,
                         PRIMARY KEY (`id`),
                         `cart_id` int not null,
                         CONSTRAINT `fk_cart` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`),
                         `book_id` int not null,
                         CONSTRAINT `fk_book` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`)
);