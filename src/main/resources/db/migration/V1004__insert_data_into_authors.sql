use bookshop;

insert into `authors`(`full_name`,`image`,`age`,`short_biography`)
values ('Stephen Edwin King', 'StephenEdwin.png', 60, 'American author of contemporary horror, fiction, suspense and fantasy.'),
       ('J.R.R. Tolkien', 'Tolkien.png', 65, 'English writer, poet, philologist, and university professor.'),
       ('Charles Dickens', 'Dickens.png', 50, 'English writer and social critic.'),
       ('J. K. Rowling', 'Rowling.png', 55, 'British novelist, screenwriter and film producer.');