package bookshop.app.Repository;


import bookshop.app.Model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Controller;

@Controller
public interface BookRepository extends JpaRepository <Book, Integer> {
    Page<Book> findAllByCategoryId(int placeId, Pageable pageable);

    @Query("select b from Book as b where b.category.id = ?2 and b.name like CONCAT('%', ?1, '%')")
    Page<Book> findAllByNameAndCategoryId(String name, int category_id, Pageable pageable);

    @Query("select b from Book as b where b.category.id = ?2 and b.name like CONCAT('%', ?1, '%')" +
            "and b.price >= ?3 and b.price <= ?4")
    Page<Book> findAllByCategoryNameMinMax(String name, int category_id, float min, float max, Pageable pageable);

    @Query("select b from Book as b where b.category.id = ?2 and b.name like CONCAT('%', ?1, '%')" +
            "and b.price >= ?3")
    Page<Book> findAllByCategoryNameMin(String name, int category_id, float min, Pageable pageable);

    @Query("select b from Book as b where b.category.id = ?2 and b.name like CONCAT('%', ?1, '%')" +
            "and b.price <= ?3")
    Page<Book> findAllByCategoryNameMax(String name, int category_id, float max, Pageable pageable);

    @Query("select b from Book as b where b.category.id = ?1 and b.price >= ?2 and b.price <= ?3")
    Page<Book> findAllByCategoryMinMax(int category_id, float min, float max, Pageable pageable);

    @Query("select b from Book as b where b.category.id = ?1 and b.price <= ?2")
    Page<Book> findAllByCategoryMax(int category_id, float max, Pageable pageable);

    @Query("select b from Book as b where b.category.id = ?1 and b.price >= ?2")
    Page<Book> findAllByCategoryMin(int category_id, float min, Pageable pageable);
}