package bookshop.app.Repository;

import bookshop.app.Model.OrdersAndBooks;
import org.springframework.data.repository.CrudRepository;

public interface OrdersAndBooksRepository extends CrudRepository<OrdersAndBooks, Integer> {
    public OrdersAndBooks findByOrderIdAndBookId(Integer userId, Integer bookId);
}
