package bookshop.app.Repository;

import bookshop.app.Model.Cart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends CrudRepository< Cart, Integer> {
    public boolean existsByUserId(Integer userId);
    public Cart findByUserId(Integer userId);
}
