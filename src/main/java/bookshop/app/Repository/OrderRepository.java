package bookshop.app.Repository;

import bookshop.app.Model.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Integer> {
    List<Order> findAllByUserId(Integer userId);
}
