package bookshop.app.Repository;

import bookshop.app.Model.CartsAndBooks;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartsBooksRepository extends CrudRepository<CartsAndBooks, Integer> {
    List<CartsAndBooks> findAllByCartId(Integer cartId);

    CartsAndBooks findByCartIdAndBookId(Integer cartId, Integer bookId);
}