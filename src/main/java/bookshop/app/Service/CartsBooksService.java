package bookshop.app.Service;

import bookshop.app.Model.CartsAndBooks;
import bookshop.app.Repository.CartsBooksRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartsBooksService {
    private final CartsBooksRepository cartsBooksRepository;

    public CartsBooksService(CartsBooksRepository cartsBooksRepository) {
        this.cartsBooksRepository = cartsBooksRepository;
    }

    public void saveCab(CartsAndBooks cartsAndBooks){
        cartsBooksRepository.save(cartsAndBooks);
    }

    public List<CartsAndBooks> getBooksByCartId(Integer cartId){
        return cartsBooksRepository.findAllByCartId(cartId);
    }

    public CartsAndBooks getByCartIdAndBookId(Integer cartId, Integer bookId){
        return cartsBooksRepository.findByCartIdAndBookId(cartId, bookId);
    }

    public boolean existByCartAndBook(Integer cartId, Integer bookId){
        var cab = cartsBooksRepository.findByCartIdAndBookId(cartId, bookId);
        return cab != null;
    }

    public void deleteBookFromCart(Integer cartId, Integer bookId){
        CartsAndBooks cab = cartsBooksRepository.findByCartIdAndBookId(cartId, bookId);
        cartsBooksRepository.delete(cab);
    }

    public void deleteCart(Integer cartId){
        List<CartsAndBooks> cabs = cartsBooksRepository.findAllByCartId(cartId);
        cartsBooksRepository.deleteAll(cabs);
    }
}
