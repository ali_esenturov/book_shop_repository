package bookshop.app.Service;

import bookshop.app.Model.Review;
import bookshop.app.Repository.ReviewRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewService {
    private final ReviewRepository reviewRepository;

    public ReviewService(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public List<Review> getReviewsByBook(Integer bookId){
        return reviewRepository.findAllByBookId(bookId);
    }

    public void saveReview(Review review){
        reviewRepository.save(review);
    }
}
