package bookshop.app.Service;


import bookshop.app.Exceptions.UserAlreadyRegistered;
import bookshop.app.Exceptions.UserNotFoundException;
import bookshop.app.Model.User;
import bookshop.app.Model.UserRegisterForm;
import bookshop.app.Model.UserResponseDTO;
import bookshop.app.Repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public UserResponseDTO getByEmail(String email) {
        var user = userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);

        return UserResponseDTO.from(user);
    }

    public User findUserByEmail(String email){
        return userRepository.findByEmail(email).get();
    }

    public UserResponseDTO register(UserRegisterForm form) {
        if (userRepository.existsUserByEmail(form.getEmail())) {
            throw new UserAlreadyRegistered();
        }

        var user = User.builder()
                .email(form.getEmail())
                .name(form.getName())
                .password(encoder.encode(form.getPassword()))
                .birthDate(form.getBirthDate())
                .build();

        userRepository.save(user);

        return UserResponseDTO.from(user);
    }
}