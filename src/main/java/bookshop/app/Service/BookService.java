package bookshop.app.Service;

import bookshop.app.Model.Book;
import bookshop.app.Model.BookDTO;
import bookshop.app.Repository.BookRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class BookService {
    private final BookRepository br;

    public Page<Book> getBooks(int id, Pageable pageable) {
        return br.findAllByCategoryId(id, pageable);
    }

    public List<Book> findAll(){
        return br.findAll();
    }

    public Book findById(Integer book_id){
        return br.findById(book_id).get();
    }

    public void saveBook(Book book){
        br.save(book);
    }

    public Page<BookDTO> searchByCategoryNameMinMax(String name, int category_id, String min, String max, Pageable pageable){
        Page<Book> books;

        if(!name.isBlank() && !max.isBlank() && !min.isBlank()){
            books = br.findAllByCategoryNameMinMax(name,category_id,Float.parseFloat(min), Float.parseFloat(max), pageable);
        }
        else if(!name.isBlank() && !min.isBlank()){
            books = br.findAllByCategoryNameMin(name,category_id,Float.parseFloat(min), pageable);
        }
        else if(!name.isBlank() && !max.isBlank()){
            books = br.findAllByCategoryNameMax(name,category_id,Float.parseFloat(max),pageable);
        }
        else if(!min.isBlank() && !max.isBlank()){
            books = br.findAllByCategoryMinMax(category_id,Float.parseFloat(min), Float.parseFloat(max), pageable);
        }
        else if(!min.isBlank()){
            books = br.findAllByCategoryMin(category_id, Float.parseFloat(min), pageable);
        }
        else if(!max.isBlank()){
            books = br.findAllByCategoryMax(category_id, Float.parseFloat(max), pageable);
        }
        else if(!name.isBlank()){
            books = br.findAllByNameAndCategoryId(name, category_id, pageable);
        }
        else{
            books = br.findAllByCategoryId(category_id, pageable);
        }
        return books.map(BookDTO::from);
    }
}