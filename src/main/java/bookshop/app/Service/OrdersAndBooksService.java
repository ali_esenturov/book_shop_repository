package bookshop.app.Service;

import bookshop.app.Model.OrdersAndBooks;
import bookshop.app.Repository.OrdersAndBooksRepository;
import org.springframework.stereotype.Service;

@Service
public class OrdersAndBooksService {
    private final OrdersAndBooksRepository ordersAndBooksRepository;

    public OrdersAndBooksService(OrdersAndBooksRepository ordersAndBooksRepository) {
        this.ordersAndBooksRepository = ordersAndBooksRepository;
    }

    public void saveOrderAndBook(OrdersAndBooks oab){
        ordersAndBooksRepository.save(oab);
    }

    public boolean existByOrderAndBook(Integer orderId, Integer bookId){
        var oab = ordersAndBooksRepository.findByOrderIdAndBookId(orderId, bookId);
        return oab != null;
    }
}
