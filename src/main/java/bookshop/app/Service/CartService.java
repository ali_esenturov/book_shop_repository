package bookshop.app.Service;

import bookshop.app.Model.Cart;
import bookshop.app.Repository.CartRepository;
import org.springframework.stereotype.Service;

@Service
public class CartService {
    final private CartRepository cartRepository;

    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public void saveCart(Cart cart){
        cartRepository.save(cart);
    }

    public boolean existByUserId(Integer userId){
        return cartRepository.existsByUserId(userId);
    }

    public Cart getByUserId(Integer userId){
        return cartRepository.findByUserId(userId);
    }
}
