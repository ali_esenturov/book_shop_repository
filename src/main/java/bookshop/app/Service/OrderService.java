package bookshop.app.Service;

import bookshop.app.Model.Order;
import bookshop.app.Repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void saveOrder(Order order){
        orderRepository.save(order);
    }

    public List<Order> getOrdersByUser(Integer userId){
        return orderRepository.findAllByUserId(userId);
    }
}
