package bookshop.app.Service;

import bookshop.app.Model.CategoryDTO;
import bookshop.app.Repository.BookCategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CategoryService {
    private final BookCategoryRepository bookCategoryRepository;

    public Page<CategoryDTO> getCategories(Pageable pageable) {
        return bookCategoryRepository.findAll(pageable)
                .map(CategoryDTO::from);
    }

    public CategoryDTO getCategory(int id) {
        return CategoryDTO.from(bookCategoryRepository.findById(id).get());
    }
}