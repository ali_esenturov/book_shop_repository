package bookshop.app.Model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "orders_books")
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class OrdersAndBooks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ToString.Exclude
    @ManyToOne
    private Order order;

    @ToString.Exclude
    @ManyToOne
    private Book book;

    @Column
    private int amount;

    @Column(name="order_time")
    private LocalDateTime timeOfOrder;
}
