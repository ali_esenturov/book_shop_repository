package bookshop.app.Model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "carts_books")
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class CartsAndBooks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ToString.Exclude
    @ManyToOne
    private Cart cart;

    @ToString.Exclude
    @ManyToOne
    private Book book;

    @Column
    private int amount;
}
