package bookshop.app.Model;

import lombok.*;

@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CategoryDTO {
    private Integer id;
    private String name;

    public static CategoryDTO from(BookCategory bookCategory) {
        return builder()
                .id(bookCategory.getId())
                .name(bookCategory.getName())
                .build();
    }
}
