package bookshop.app.Model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BookDTO {
    private Integer id;
    private String name;
    private String image;
    private float price;
    private String author_name;
    private int category_id;

    public static BookDTO from(Book book) {
        return builder()
                .id(book.getId())
                .name(book.getName())
                .image(book.getImage())
                .price(book.getPrice())
                .author_name(book.getAuthor().getFullName())
                .category_id(book.getCategory().getId())
                .build();
    }
}
