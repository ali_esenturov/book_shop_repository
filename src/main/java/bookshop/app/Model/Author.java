package bookshop.app.Model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "authors")
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128)
    private String fullName;

    @Column(length = 128)
    private String image;

    @Column
    private int age;

    @Column
    private String shortDescription;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "author")
    @OrderBy("name ASC")
    List<Book> books;
}