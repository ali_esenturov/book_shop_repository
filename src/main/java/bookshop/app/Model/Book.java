package bookshop.app.Model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@Table(name = "books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128)
    private String name;

    @Column(length = 250)
    private String description;

    @Column(length = 128)
    private String image;

    @Column
    private float price;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "bookCategory_id")
    private BookCategory category;

    @ToString.Exclude
    @ManyToOne
    private Author author;
}