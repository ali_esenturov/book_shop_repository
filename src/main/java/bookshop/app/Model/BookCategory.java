package bookshop.app.Model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "book_categories")
public class BookCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128)
    private String name;

    @Column()
    private String description;

    @OneToMany(mappedBy = "category")
    @OrderBy("name ASC")
    List<Book> books;
}