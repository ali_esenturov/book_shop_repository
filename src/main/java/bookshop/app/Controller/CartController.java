package bookshop.app.Controller;

import bookshop.app.Model.*;
import bookshop.app.Service.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class CartController {
    private final UserService userService;
    private final CartService cartService;
    private final BookService bookService;
    private final CartsBooksService cartsBooksService;


    public CartController(UserService userService, CartService cartService, BookService bookService,
                          CartsBooksService cartsBooksService) {
        this.userService = userService;
        this.cartService = cartService;
        this.bookService = bookService;
        this.cartsBooksService = cartsBooksService;
    }

    @GetMapping("/cart")
    public String cart(Model model, @SessionAttribute(name = Constants.CART_ID, required = false) List<String> cart) {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        User u = userService.findUserByEmail(authentication.getName());
        int user_id = u.getId();

        Cart c = cartService.getByUserId(user_id);

        List<CartsAndBooks> cabs = cartsBooksService.getBooksByCartId(c.getId());
        model.addAttribute("cabs", cabs);
        model.addAttribute("cart_id", c.getId());
        return "cart";
    }

    @PostMapping("/cart")
    @ResponseBody
    public boolean addToListRest(@RequestParam String item_id,
                                 @SessionAttribute(name = Constants.CART_ID, required = false) List<String> cart) {
        if (cart != null) {
            cart.add("id:" + item_id + "-" + "amount:1");
        }

        var authentication = SecurityContextHolder.getContext().getAuthentication();
        User u = userService.findUserByEmail(authentication.getName());
        int user_id = u.getId();
        int book_id = Integer.parseInt(item_id);
        CartsAndBooks cab = new CartsAndBooks();

        if(cartService.existByUserId(user_id)){
            Cart userCart = cartService.getByUserId(user_id);
            if(cartsBooksService.existByCartAndBook(userCart.getId(), book_id)){
                cab = cartsBooksService.getByCartIdAndBookId(userCart.getId(), book_id);
                cab.setAmount(cab.getAmount() + 1);
                cartsBooksService.saveCab(cab);
            }
            else{
                saveNewCab(cab, book_id, userCart);
            }
        }
        else{
            Cart newCart = new Cart();
            newCart.setUser(u);
            cartService.saveCart(newCart);
            saveNewCab(cab, book_id, newCart);
        }
        return true;
    }

    private void saveNewCab(CartsAndBooks cab, Integer book_id, Cart cart){
        cab.setAmount(1);
        cab.setBook(bookService.findById(book_id));
        cab.setCart(cart);
        cartsBooksService.saveCab(cab);
    }
}