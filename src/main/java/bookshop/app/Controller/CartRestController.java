package bookshop.app.Controller;

import bookshop.app.Model.*;
import bookshop.app.Service.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class CartRestController {
    private final CartsBooksService cartsBooksService;
    private final OrderService orderService;
    private final OrdersAndBooksService ordersAndBooksService;

    public CartRestController(CartsBooksService cartsBooksService, OrderService orderService, OrdersAndBooksService ordersAndBooksService) {
        this.cartsBooksService = cartsBooksService;
        this.orderService = orderService;
        this.ordersAndBooksService = ordersAndBooksService;
    }

    @PostMapping("/cart/change/{cart_id}/{book_id}/{action}")
    public void changeToCart(@PathVariable("cart_id") Integer cart_id, @PathVariable("book_id") Integer book_id,
                             @PathVariable("action") String action){
        CartsAndBooks cab = cartsBooksService.getByCartIdAndBookId(cart_id, book_id);
        if(action.equals("plus")){
            int newAmount = cab.getAmount() + 1;
            cab.setAmount(newAmount);
        }
        else{
            int newAmount = cab.getAmount() - 1;
            cab.setAmount(newAmount);
        }
        cartsBooksService.saveCab(cab);
    }

    @PostMapping("cart/delete/{cart_id}/{book_id}")
    public void deleteBookFromCart(@PathVariable("cart_id") Integer cart_id, @PathVariable("book_id") Integer book_id){
        cartsBooksService.deleteBookFromCart(cart_id, book_id);
    }

    @PostMapping("/cart/make/order/{cart_id}")
    public void makeOrder(HttpSession session, @PathVariable("cart_id") Integer cart_id) {
        session.removeAttribute(Constants.CART_ID);

        List<CartsAndBooks> cabs = cartsBooksService.getBooksByCartId(cart_id);
        User user = cabs.get(0).getCart().getUser();
        Order order = new Order();
        order.setUser(user);
        orderService.saveOrder(order);

        for(CartsAndBooks cab : cabs){
            int amount = cab.getAmount();
            Book book = cab.getBook();
            LocalDateTime timeOfOrder = LocalDateTime.now();

            OrdersAndBooks oab = new OrdersAndBooks();
            oab.setAmount(amount);
            oab.setOrder(order);
            oab.setBook(book);
            oab.setTimeOfOrder(timeOfOrder);

            ordersAndBooksService.saveOrderAndBook(oab);
        }
        cartsBooksService.deleteCart(cart_id);
    }
}
