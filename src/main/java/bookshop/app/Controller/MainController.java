package bookshop.app.Controller;

import bookshop.app.Model.Book;
import bookshop.app.Model.Order;
import bookshop.app.Model.Review;
import bookshop.app.Model.User;
import bookshop.app.Service.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class MainController {

    private final BookService bookService;
    private final CategoryService categoryService;
    private final PropertiesService propertiesService;
    private final UserService userService;
    private final OrdersAndBooksService ordersAndBooksService;
    private final OrderService orderService;
    private final ReviewService reviewService;

    public MainController(BookService bookService, CategoryService categoryService, PropertiesService propertiesService,
                          UserService userService, OrdersAndBooksService ordersAndBooksService, OrderService orderService,
                          ReviewService reviewService) {
        this.bookService = bookService;
        this.categoryService = categoryService;
        this.propertiesService = propertiesService;
        this.userService = userService;
        this.ordersAndBooksService = ordersAndBooksService;
        this.orderService = orderService;
        this.reviewService = reviewService;
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink",
                    constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink",
                    constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }

    @GetMapping
    public String index(Model model, Pageable pageable, HttpServletRequest uriBuilder) {
        var places = categoryService.getCategories(pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(places, propertiesService.getDefaultPageSize(), model, uri);
        return "categories";
    }

    @PostMapping("/categories/search")
    public String getSearchResult(@RequestParam("category_id") int category_id,
                                  @RequestParam("name") String name,
                                  @RequestParam("min_price") String min_price,
                                  @RequestParam("max_price") String max_price) {
        String format = "/categories/search/category=%s/name=%s/min=%s/max=%s";
        return "redirect:" + String.format(format, category_id, name, min_price, max_price);
    }

    @GetMapping("/categories/search/category={category_id}/name={name}/min={min_price}/max={max_price}")
    public String showResultWithNameMinMax(Model model, Pageable pageable, HttpServletRequest uriBuilder,
                                           @PathVariable("name") String name, @PathVariable("category_id") int id,
                                           @PathVariable("min_price") String min, @PathVariable("max_price") String max){
        model.addAttribute("category", categoryService.getCategory(id));
        model.addAttribute("user_status", getUserStatus());
        var uri = uriBuilder.getRequestURI();
        var books = bookService.searchByCategoryNameMinMax(name, id, min, max, pageable);
        constructPageable(books, propertiesService.getDefaultPageSize(), model, uri);
        return "books";
    }

    @GetMapping("/categories/{id:\\d+?}")
    public String categoryPage(@PathVariable int id, Model model, Pageable pageable, HttpServletRequest uriBuilder) {
        model.addAttribute("category", categoryService.getCategory(id));
        model.addAttribute("user_status", getUserStatus());
        var uri = uriBuilder.getRequestURI();
        var books = bookService.getBooks(id, pageable);
        constructPageable(books, propertiesService.getDefaultPageSize(), model, uri);
        return "books";
    }

    @GetMapping("/books/{book_id}")
    public String bookPage(@PathVariable("book_id") Integer book_id, Model model){
        Book book = bookService.findById(book_id);
        model.addAttribute("book", book);
        model.addAttribute("user_status", getUserStatus());
        model.addAttribute("bought", bought(book_id));
        model.addAttribute("reviews", reviewService.getReviewsByBook(book_id));
        return "book";
    }

    @PostMapping("/add/review")
    public String addReview(@RequestParam("review") String text,
                            @RequestParam("book_id") Integer bookId){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName();
        User user = userService.findUserByEmail(email);
        Book book = bookService.findById(bookId);

        Review review = new Review();
        review.setBook(book);
        review.setUser(user);
        review.setText(text);

        reviewService.saveReview(review);
        return "redirect:/books/" + bookId;
    }

    private String getUserStatus(){
        String user_status = "";
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        try{
            user_status = (String)authentication.getPrincipal();
        }
        catch (Exception ex){
            user_status = "authorizedUser";
        }
        return user_status;
    }

    private boolean bought(Integer book_id){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName();
        if(email.equals("anonymousUser")){
            return false;
        }
        else{
            User user = userService.findUserByEmail(email);
            List<Order> orders = orderService.getOrdersByUser(user.getId());
            for(Order o : orders){
                if(ordersAndBooksService.existByOrderAndBook(o.getId(), book_id)){
                    return true;
                }
            }
            return false;
        }
    }
}