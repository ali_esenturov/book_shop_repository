package bookshop.app.Controller;


import bookshop.app.Model.ReCaptureResponse;
import bookshop.app.Model.UserRegisterForm;
import bookshop.app.Service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping
@AllArgsConstructor
public class UserController {

    private final UserService userService;
    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/profile")
    public String pageCustomerProfile(Model model, Principal principal) {
        var user = userService.getByEmail(principal.getName());
        model.addAttribute("dto", user);
        return "profile";
    }

    @GetMapping("/register")
    public String pageRegisterCustomer(Model model) {
        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new UserRegisterForm());
        }
        return "register";
    }

    @PostMapping("/register")
    public String registerPage(@Valid UserRegisterForm userRequestDTO,
                               BindingResult validationResult,
                               RedirectAttributes attributes,
                               @RequestParam("g-recaptcha-response") String captureResponse) {

        String url = "https://www.google.com/recaptcha/api/siteverify";
        String params = "?secret=6LesUvYUAAAAAEPNx3zYBqlpbNmFZtX5NazCfXXO&response=" + captureResponse;

        ReCaptureResponse reCaptureResponse = restTemplate.exchange(url+params, HttpMethod.POST ,null, ReCaptureResponse.class).getBody();

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/register";
        }
        if(reCaptureResponse.isSuccess()){
            userService.register(userRequestDTO);
            return "redirect:/login";
        }
        else{
            return "redirect:/register";
        }
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }
}