package bookshop.app.Controller;


import bookshop.app.Model.Book;
import bookshop.app.Model.BookCategory;
import bookshop.app.Model.BookDTO;
import bookshop.app.Model.CategoryDTO;
import bookshop.app.Service.BookService;
import bookshop.app.Service.CategoryService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class CategoryRestController {

    private final BookService bs;
    private final CategoryService cs;

    @GetMapping
    public List<CategoryDTO> getCategories(Pageable pageable) {
        return cs.getCategories(pageable).getContent();
    }

    @GetMapping("/{id:\\d+?}")
    public CategoryDTO getCategory(@PathVariable int id) {
        return cs.getCategory(id);
    }

    @GetMapping("/{id:\\d+}/books")
    public List<Book> getBooks(@PathVariable @Min(3) int id, Pageable pageable) {
        return bs.getBooks(id, pageable).getContent();
    }
}
